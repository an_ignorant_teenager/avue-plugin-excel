const path = require('path');
const resolve = p => {
  const base = p.split('/')[0];
  return path.resolve(__dirname, './', p);
};
module.exports = {
  mode: 'production',
  entry: resolve('src/index.js'),
  output: {
    path: resolve('dist'),
    filename: 'index.js',
    libraryExport: "default",
    libraryTarget: 'umd',
    library: '$Excel',
    umdNamedDefine: true,
    globalObject: 'this'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: process.cwd(),
        exclude: /node_modules/,
        loader: 'babel-loader'
      }]
  },
  //插件，用于生产模板和各项功能
  plugins: [],
  //配置webpack开发服务器功能
  devServer: {}
}